<?php declare(strict_types = 1);
require_once "autoload.php";
$authentication = new SecureUserAuthentication();
$user = $authentication->getUser();

//Requêtes SQL
$recupFriend = MyPDO::getInstance()->prepare(<<<SQL
	SELECT use_idUser as "id"
	FROM avoir_en_ami
	WHERE idUser = :id
SQL);
$recupFriend->execute([":id" => $user->getIdUser()]);
$Friends = "";
foreach($recupFriend->fetchAll() as $k){
    $Friend = User::createFromId($k['id']);
    $image = $Friend->getIdImage();
    $nom = $Friend->getName();
    $Friends .= <<<HTML
        <div class="flex-fill p-2 border mx-1 my-1" onclick="addFriend(this);" id="{$Friend->getIdUser()}">
            <img src="getImage.php?id=$image" width="70" height="70" title="$nom"> $nom
        </div>
HTML;
}
//Fin Requêtes SQL

echo $Friends;
