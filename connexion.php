<?php declare(strict_types=1);
require_once "autoload.php";
if (isset($_POST["code"])) {
	$authentication = new SecureUserAuthentication();
	$user = $authentication->getUserFromAuth();
	http_response_code(308);
    header("Location: index.php");
    die();
}
else {
Session::start();
$challenge = Random::string(16);
$_SESSION[AbstractUserAuthentication::SESSION_KEY]["challenge"] = $challenge;

$page = new WebPage("Connexion");
$page->appendToHead(<<<HTML
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/connexion.css" rel="stylesheet">
HTML);
$page->appendContent(<<<HTML
<div class="d-flex flex-grow-1 flex-column mx-2 my-2 flex-md-row">
	<span class="flex-fill p-2 "><img src="img/ordinateur.png" alt="ordinateur"></span>

	<div class="flex-fill flex-row p-2 mx-1 my-1">
		<form name="connexion" method="POST" action="connexion.php">
			<div class="flex-fill flex-row p-2 border mx-5 mt-2">
				<p class="d-flex justify-content-center GAKALL">GAKALL Sport</span>
				<div class="d-flex flex-row mx-5 mt-2 justify-content-center">
					<label class="logmdp">
						<input id="login" type="text" placeholder="Nom d'utilisateur ou e-mail" required>
					</label>
				</div>
				<div class="d-flex flex-row mx-5 mt-2 justify-content-center">
					<label class="logmdp">
						<input id="pass" type="password" placeholder="Mot de passe" required>
						<input for="checkbox" id="checkbox" type="checkbox">

						<input hidden type='text' name="code" id="code" value="">
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
						<script type="text/javascript">
							$(document).ready(function() {
								var checkbox = $("#checkbox");
								var password = $("#pass");
								checkbox.click(function() {
									if (checkbox.prop("checked")) {
										password.attr("type", "text");
									} else {
										password.attr("type", "password");
									}
								});
							});
						</script>
					</label>
				</div>
				<div class="d-flex flex-column m-5 justify-content-center button">
					<button class="mx-5 mt-2" type="submit">Connexion</button>
				</div>
				<input hidden type='text' id="challenge" value="{$challenge}">
				<input hidden type='text' id="code" value="">
		</form>

		<script src="js/sha512.js" type="text/javascript"></script>
		<script>
			window.onload = function() {
				document.addEventListener("submit", hashCode);
			}

			function hashCode() {
				var login = CryptoJS.SHA512(document.querySelector("#login").value);
				var pass = CryptoJS.SHA512(document.querySelector("#pass").value);
				var challenge = document.querySelector("#challenge").value;
				var code = document.querySelector("#code");
				code.value = CryptoJS.SHA512(login + pass + challenge);
			}
		</script>

	</div>
	<div class="mx-5 mt-2 d-flex flex-fill border justify-content-center">
		<p class="textnoire">Pas de compte ?</p>
		<a href="inscription.html" class="textorange">Inscrivez-vous</a>
	</div>
	<div class="mx-5 mt-2 d-flex flex-column justify-content-center">
		<p class="d-flex justify-content-center textnoire">Télécharger l'application</p>
		<div class="flex-row d-flex justify-content-center">
			<img src="img/app.png" alt="appstore">
			<img src="img/google.png" alt="appstore">
		</div>
	</div>
</div>
HTML);

echo $page->toHTML();
}
