<?php declare(strict_types=1);
require_once "autoload.php";
$authentication = new SecureUserAuthentication();
if (!$authentication->isUserConnected()) {
	http_response_code(401);
    header("Location: connexion.php");
    die();
}
$p = new WebPage("Page d'accueil");
$p->appendJsUrl("js/ajaxrequest.js");
$p->appendJsUrl("js/posts.js");
$p->appendCssUrl("css/main.css");
$p->appendToHead(<<<HTML
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@700&display=swap"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto&display=swap">
HTML);

$user = $authentication->getUser();
$header = SportUtilities::getHeaderPage($user);

$p->appendContent(<<<HTML
$header
<div class="d-flex flex-row main-categories">
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/Volley-ball.png" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Volleyball</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/basket.jpg" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Basketballl</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/soccer.jpg" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Soccer</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/bowling.jpg" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Bowling</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/badminton.png" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Badminton</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/run.jpg" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Course à pied</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/muscle.jpg" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Musculation</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/dance.jfif" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Danse</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/lutte.jpg" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Lutte</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/Boxe.png" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Boxe</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/rugby.jfif" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Rugby</div>
	</div>
	<div class="d-flex flex-column scroll-item m-3">
		<img src="img/climb.png" class="scroll-item-img" alt="Icon catégorie">
		<div class="scroll-item-title">Escalade</div>
	</div>
</div>
HTML);

$search = "";
if (isset($_GET["search"])) {
	$search = $_GET["search"];
}
$posts = SportUtilities::getPublications($search);

$p->appendContent(<<<HTML
<div class="main-post" id="listePublications">
$posts
</div>
HTML);

echo $p->toHTML();
